import * as actionTypes from '../actions/actionTypes';
import { updateObject } from '../../shared/utility';

export const initialState = {
  properties: [],
  loading: false
};

const fetchPropertiesStart = ( state ) => {
  return updateObject( state, { loading: true } );
};

const fetchPropertiesSuccess = ( state, action ) => {
  return updateObject( state, {
    properties: action.properties,
    loading: false
  } );
};

const fetchPropertiesFail = ( state ) => {
  return updateObject( state, { loading: false } );
};

const selectProperty = ( state, action ) => {
  const updatedProperties = state.properties.map((property, propertyKey) => {
    if (propertyKey === action.key) {
      if (!property.hasOwnProperty('selected') || !property.selected) {
        property.selected = true;

        return property;
      }
    }
    
    property.selected = false;
    return property;
  });

  return updateObject(state, {properties: updatedProperties});
};

export const reducer = ( state = initialState, action ) => {
  switch ( action.type ) {
  case actionTypes.FETCH_PROPERTIES_START: return fetchPropertiesStart( state, action );
  case actionTypes.FETCH_PROPERTIES_SUCCESS: return fetchPropertiesSuccess( state, action );
  case actionTypes.FETCH_PROPERTIES_FAIL: return fetchPropertiesFail( state, action );
  case actionTypes.SELECT_PROPERTY: return selectProperty( state, action );
  default: return state;
  }
};

export default reducer;