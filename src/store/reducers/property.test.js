import {reducer, initialState} from './property';
import * as actionTypes from '../actions/actionTypes';


describe ('property reducer', () => {

  let propertyLondon1, propertyLondon2;
  beforeEach(() => {
    propertyLondon1 = {
      'owner': 'carlos',
      'address': {
        'line1': 'Flat 5',
        'line4': '7 Westbourne Terrace',
        'postCode': 'W2 3UL',
        'city': 'London',
        'country': 'U.K.'
      },
      'airbnbId': 3512500,
      'numberOfBedrooms': 1,
      'numberOfBathrooms': 1,
      'incomeGenerated': 2000.34,
      'selected': true
    };

    propertyLondon2 = {
      'owner': 'ankur',
      'address': {
        'line1': '16 Ellerdine Rd, Hounslow TW3 2PL, UK'
      },
      'airbnbId': 1334159,
      'numberOfBedrooms': 3,
      'numberOfBathrooms': 1,
      'incomeGenerated': 10000,
      'selected': false
    };
  });
  
  it ('should return initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState);
  });

  it ('should be loading when starting to fetch properties', () => {
    expect(reducer(undefined, {type: actionTypes.FETCH_PROPERTIES_START})).toEqual({
      properties: [],
      loading: true
    });
  }); 

  it ('should stop loading and set properties after fetch success', () => {
    expect(reducer({loading: true, properties: []}, {type: actionTypes.FETCH_PROPERTIES_SUCCESS, properties: [propertyLondon1, propertyLondon2]})).toEqual({
      properties: [propertyLondon1, propertyLondon2],
      loading: false
    });
  });
  
  it ('should stop loading after fetch fail', () => {
    expect(reducer({loading: true, properties: []}, {type: actionTypes.FETCH_PROPERTIES_FAIL})).toEqual({
      properties: [],
      loading: false
    });
  });

  it ('should select property1 and unselect property2', () => {
    expect(reducer({loading: false, properties: [propertyLondon1, propertyLondon2]}, {type: actionTypes.SELECT_PROPERTY, key:1})).toEqual({
      properties: [{...propertyLondon1, selected: false}, {...propertyLondon2, selected:true}],
      loading: false
    });
  }); 
});