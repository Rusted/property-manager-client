import * as actionTypes from './actionTypes';
import axios from '../../axios-properties';

export const fetchPropertiesSuccess = ( properties ) => {
  return {
    type: actionTypes.FETCH_PROPERTIES_SUCCESS,
    properties: properties
  };
};

export const fetchPropertiesFail = ( error ) => {
  return {
    type: actionTypes.FETCH_PROPERTIES_FAIL,
    error: error
  };
};

export const fetchPropertiesStart = () => {
  return {
    type: actionTypes.FETCH_PROPERTIES_START
  };
};

export const fetchproperties = () => {
  return dispatch => {
    dispatch(fetchPropertiesStart());
    axios.get( '/properties')
      .then( res => {
        dispatch(fetchPropertiesSuccess(res.data));
      })
      .catch( err => {
        dispatch(fetchPropertiesFail(err));
      })
    ;
  };
};

export const selectProperty = (key) => {
  return {
    type: actionTypes.SELECT_PROPERTY,
    key
  };
};