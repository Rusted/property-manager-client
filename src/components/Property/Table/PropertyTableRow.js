import React from 'react';

const PropertyTableRow = (props) => {
  const addressKeys = Object.keys(props.address);
  let addresses = '';
  addresses = addressKeys.map((addressKey) => (
    <li key={props.propertyKey + '_' + addressKey}>{props.address[addressKey]}</li>
  ));

  if (addresses.length > 0) {
    addresses = <ul>{addresses}</ul>;
  }

  let className='property-row';
  if (props.selected === true) {
    className+=' selected';
  }
        
  return <tr className={className} key={props.propertyKey} onClick={() => props.onSelect(props.propertyKey)}>
    <td>{props.owner}</td>
    <td>{addresses}</td>
    <td>{props.numberOfBathrooms}</td>
    <td>{props.numberOfBedrooms}</td>
    <td>
      <a href={'https://www.airbnb.com/rooms/' + props.airbnbId } target="_blank">
        Link
      </a>
    </td>
    <td>{props.inArea ? 'yes' : 'no'}</td>
    <td>{props.incomeGenerated.toFixed(2)}&nbsp;&euro;</td>
  </tr>;   
};

export default PropertyTableRow;