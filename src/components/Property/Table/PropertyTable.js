import React from 'react';
import PropertyTableRow from './PropertyTableRow';

const PropertyTable = (props) => (
  <table className="table">
    <thead>
      <tr>
        <th>Owner</th>
        <th>Address</th>
        <th>Bedrooms</th>
        <th>Bathrooms</th>
        <th>Link</th>
        <th>Service area</th>
        <th>Income Generated</th>
      </tr>
    </thead>
    <tbody>
      {props.properties.map((property, key) => {       
        return <PropertyTableRow
          owner={property.owner}
          address={property.address}
          numberOfBathrooms={property.numberOfBathrooms}
          numberOfBedrooms={property.numberOfBedrooms}
          airbnbId={property.airbnbId}
          incomeGenerated={property.incomeGenerated}
          key={key}
          propertyKey={key}
          onSelect={props.onSelect}
          selected={property.selected}
          inArea={property.inArea}

        />; 
      })}
    </tbody>
  </table>
);

export default PropertyTable;