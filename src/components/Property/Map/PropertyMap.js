import React from 'react';
import { withScriptjs, withGoogleMap, GoogleMap, Circle } from 'react-google-maps';
import PropertyMarker from './PropertyMarker';

const PropertyMap = withScriptjs(withGoogleMap((props) =>
{

  return <GoogleMap
    defaultZoom={10}
    defaultCenter={props.serviceAreaCoordinates}
  >
    <Circle 
      center={props.serviceAreaCoordinates} 
      radius={props.serviceAreaRadius}
      options={{strokeWeight: 1, scale: 1.0}}
    />
    {props.properties ? props.properties.map((property, propertyKey) => {
      return <PropertyMarker 
        property={property} 
        key={propertyKey}
        propertyKey={propertyKey}
        onSelect={props.onSelect}
      />;
    }): ''}
  </GoogleMap>;
}
));

export default PropertyMap;