import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const PropertyMarkerDialog = (props) => {
  return (
    <div className="map-label">
      <div className="map-label-address"><FontAwesomeIcon icon="map-marker-alt" size="lg"/>
        {props.fullAddress}
      </div>
      <div className="map-label-owner"><FontAwesomeIcon icon="portrait" size="lg"/>
        {props.owner}
      </div>
      <div className="map-label-owner"><FontAwesomeIcon icon="bed" size="lg"/>
        {props.numberOfBedrooms}
      </div>
      <div className="map-label-owner"><FontAwesomeIcon icon="bath" size="lg"/>
        {props.numberOfBathrooms}
      </div>
      <div className="map-label-owner"><FontAwesomeIcon icon="link" size="lg"/>
        <a href={'https://www.airbnb.com/rooms/' + props.airbnbId } target="_blank">
            Link
        </a>
      </div>
      <div className="map-label-owner"><FontAwesomeIcon icon="money-bill-alt" size="lg"/>
        {props.incomeGenerated.toFixed(2)}&nbsp;&euro;
      </div>
    </div>
  );
};

export default PropertyMarkerDialog;