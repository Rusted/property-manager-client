import React from 'react';
import MarkerWithLabel from 'react-google-maps/lib/components/addons/MarkerWithLabel';
import PropertyMarkerDialog from './PropertyMarkerDialog';

const PropertyMarker = (props) => {
  let icon;
  let className = 'map-marker';
  if (props.property.selected === true) {
    icon = {url: 'img/marker-green.png', scaledSize: {height: 40, width: 25}};
    className+= ' selected';
  }

  return (
    <MarkerWithLabel
      icon={icon}
      className={className}
      position={props.property.location}
      labelAnchor={{x: -20, y: 100}}
      clickable={true}
      labelVisible={props.property.selected === true}
      onClick={(e) => {
        /** some hacks for not working link in map dialog */
        if (e.target && e.target.tagName.toLowerCase() === 'a') {
          window.open(e.target.href, '_blank');
        } else {
          props.onSelect(props.propertyKey);
        }
      }}
      zIndex={10}
    >
      <PropertyMarkerDialog
        fullAddress={props.property.fullAddress}
        owner={props.property.owner}
        numberOfBedrooms={props.property.numberOfBedrooms}
        numberOfBathrooms={props.property.numberOfBathrooms}
        airbnbId={props.property.airbnbId}
        incomeGenerated={props.property.incomeGenerated}
      />
    </MarkerWithLabel>
  );
};

export default PropertyMarker;