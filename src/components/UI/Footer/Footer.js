import React from 'react';

const footer = () => {
  return (
    <footer className="footer">
      Made by Daumantas Urbanavičius - <a href="http://www.daumantasurb.com" target="_blank" rel="noopener noreferrer">www.daumantasurb.com</a>
    </footer>);
};

export default footer;
