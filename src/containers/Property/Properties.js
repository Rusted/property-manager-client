import React, { Component } from 'react';
import { connect } from 'react-redux';
import config from '../../config';

import axios from '../../axios-properties';
import WithErrorHandler from '../../hoc/WithErrorHandler/WithErrorHandler';
import * as actions from '../../store/actions/index';
import Spinner from '../../components/UI/Spinner/Spinner';
import PropertyTable from '../../components/Property/Table/PropertyTable';
import PropertyMap from '../../components/Property/Map/PropertyMap';
import Aux from '../../hoc/Aux/Aux';

class Properties extends Component {
  componentDidMount () {
    this.props.onFetchProperties();
  }

  render () {
    let content = <Spinner />;
    if ( !this.props.loading ) {
      content = (
        <Aux>
          <div className="property-table-wrapper">
            <PropertyTable 
              properties={this.props.properties}
              onSelect={this.props.onSelect}
            />
          </div>
          <PropertyMap 
            isMarkerShown
            serviceAreaRadius={config.MAP_SERVICE_AREA_RADIUS}
            serviceAreaCoordinates={config.MAP_SERVICE_AREA_COORDINATES}
            googleMapURL={'https://maps.googleapis.com/maps/api/js?key=' + config.GOOGLE_MAPS_API_KEY}
            loadingElement={<div className="map-loading" />}
            containerElement={<div className="map-container" />}
            mapElement={<div className="map-map" />}
            properties={this.props.properties}
            onSelect={this.props.onSelect}
          />
        </Aux>
      );
    }
    return (
      <div>
        <h1>Properties</h1>
        {content}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    properties: state.property.properties,
    loading: state.property.loading,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    onFetchProperties: () => dispatch( actions.fetchproperties() ),
    onSelect: (key) => dispatch( actions.selectProperty(key) )
  };
};

export default connect( mapStateToProps, mapDispatchToProps )( WithErrorHandler( Properties, axios ) );