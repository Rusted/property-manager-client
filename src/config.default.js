export default {
  BACKEND_BASE_URL: 'http://localhost:3001/',
  GOOGLE_MAPS_API_KEY: 'your api key',
  MAP_SERVICE_AREA_COORDINATES: { lat: 51.5073835, lng: -0.1277801 },
  MAP_SERVICE_AREA_RADIUS: 20000,
  MAP_SERVICE_AREA_COUNTRY_CODE: 'gb'
};