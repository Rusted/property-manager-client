import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

import { library } from '@fortawesome/fontawesome-svg-core';
import { fab } from '@fortawesome/free-brands-svg-icons';
import { faPortrait , faMapMarkerAlt, faBed, faBath, faLink, faMoneyBillAlt} from '@fortawesome/free-solid-svg-icons';
import Properties from './containers/Property/Properties';
import Footer from './components/UI/Footer/Footer';

library.add(fab, faPortrait, faMapMarkerAlt, faBed, faBath, faLink, faMoneyBillAlt);

class App extends Component {
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Property Manager</h1>
        </header>
        <Properties/>
        <Footer />
      </div>
    );
  }
}

export default App;
