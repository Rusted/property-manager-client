# Description #
React.js SPA demo project - Property manager using Google Maps API
# Installation
1. ``cp src/config.default.js src/config.js``
2. set your Google Maps API KEY inside src/config.js
3. ``npm install``
# How to run #
``npm start``
# How to run tests #
``npm test``
# Back-end service 
https://gitlab.com/Rusted/property-manager-rest
# Author #
Daumantas Urbanavičius
